import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {Configuration} from '@core/http/configuration';
import {HttpClient} from '@angular/common/http';
import {httpInterceptorProviders} from '@core/http/intreceptor';

@NgModule({
    imports: [],
    declarations: [],
    exports: [],
    providers: [
        httpInterceptorProviders
    ]
})
export class ApiModule {
    constructor(@Optional() @SkipSelf() parentModule: ApiModule,
                @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
                'See also https://github.com/angular/angular/issues/20575');
        }
    }

    public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders<any> {
        return {
            ngModule: ApiModule,
            providers: [{provide: Configuration, useFactory: configurationFactory}]
        };
    }
}
