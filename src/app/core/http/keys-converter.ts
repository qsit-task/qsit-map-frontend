export class KeysConverter {
    public static keysToCamel(o: any) {
        if (o === Object(o) && !Array.isArray(o) && typeof o !== 'function') {
            const n = {};
            Object.keys(o)
                .forEach((k) => {
                    n[this.toCamel(k)] = this.keysToCamel(o[k]);
                });
            return n;
        } else if (Array.isArray(o)) {
            return o.map((i) => {
                return this.keysToCamel(i);
            });
        }
        return o;
    }

    private static toCamel(s: string) {
        return s.replace(/([-_][a-z])/ig, ($1) => {
            return $1.toUpperCase()
                .replace('-', '')
                .replace('_', '');
        });
    }

    public static keysToSank(o: any) {
        if (o === Object(o) && !Array.isArray(o) && typeof o !== 'function') {
            const n = {};
            Object.keys(o)
                .forEach((k) => {
                    n[this.toSank(k)] = this.keysToSank(o[k]);
                });
            return n;
        } else if (Array.isArray(o)) {
            return o.map((i) => {
                return this.keysToSank(i);
            });
        }
        return o;
    }

    private static toSank(s: string) {
        return s.replace(/([A-Z])/g, ($1) => {
            return '_' + $1.toLowerCase();
        });
    }

}
