import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SharedPreferencesService} from '@core/services';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    urlsToNotUse: Array<string>;

    constructor(private sharedPreferencesService: SharedPreferencesService) {
        this.urlsToNotUse = [
            'assets/i18n/.+'
        ];
        this.sharedPreferencesService.load().then();
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.isValidRequestForInterceptor(request.url)) {
            const token = `Bearer ${this.sharedPreferencesService.allSettings.apiToken}`;
            request = request.clone({headers: request.headers.set('Authorization', token)});
            return next.handle(request);
        }
        return next.handle(request);
    }

    private isValidRequestForInterceptor(requestUrl: string): boolean {
        for (const address of this.urlsToNotUse) {
            if (new RegExp(address).test(requestUrl)) {
                return false;
            }
        }
        return true;
    }

}
