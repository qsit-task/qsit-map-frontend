import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {KeysConverter} from '@core/http/keys-converter';

@Injectable()
export class KeysConverterInterceptor implements HttpInterceptor {

    urlsToNotUse: Array<string>;

    constructor() {
        this.urlsToNotUse = [
            'assets/i18n/.+',
            'vbp/v1/sociallogin'
        ];
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.isValidRequestForInterceptor(request.url)) {
            const requestBody = KeysConverter.keysToSank(request.body);
            request = request.clone({body: requestBody});
            return next.handle(request).pipe(
                map((event: HttpEvent<any>) => {
                    if (event instanceof HttpResponse) {
                        const newBody = KeysConverter.keysToCamel(event.body);
                        event = event.clone({body: newBody});
                        return event;
                    }
                }));
        }
        return next.handle(request);
    }

    private isValidRequestForInterceptor(requestUrl: string): boolean {
        for (const address of this.urlsToNotUse) {
            if (new RegExp(address).test(requestUrl)) {
                return false;
            }
        }
        return true;
    }

}
