export interface MapSubType {
    id?: number;
    name?: string;
    mapTypeId?: number;
}
