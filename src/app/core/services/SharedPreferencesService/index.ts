import {SharedPreferencesService} from './shared-preferences.service';
import {PreferencesModel} from './preferences.model';

export function provideSettings() {
    /**
     * The Settings provider takes a set of default settings for your app.
     *
     * You can add new settings options at any time. Once the settings are saved,
     * these values will not overwrite the saved values (this can be done manually if desired).
     */
    const preferencesModel: PreferencesModel = {
        lang: 'en',
        apiToken: '',
        apiTokenGuest: '',
        introPassed: false
    };
    return new SharedPreferencesService(preferencesModel);
}

export let UserPreferencesServiceProvider = {provide: SharedPreferencesService, useFactory: provideSettings};
export * from './shared-preferences.service';
