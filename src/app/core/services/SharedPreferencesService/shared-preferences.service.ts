import {Injectable} from '@angular/core';
import {PreferencesModel} from './preferences.model';
import {Observable} from 'rxjs';
import {LoginApiResponse} from '@core/http/models';


/**
 * A simple settings/config class for storing key/value pairs with persistence.
 */
@Injectable({
    providedIn: 'root'
})
export class SharedPreferencesService {
    private SETTINGS_KEY = '_settings';

    private settings: PreferencesModel;

    private readonly defaults: PreferencesModel;

    constructor(defaults: PreferencesModel) {
        this.defaults = defaults;
    }

    async load() {
        const settings = JSON.parse(localStorage.getItem(this.SETTINGS_KEY));
        if (settings) {
            this.settings = settings;
            return this._mergeDefaults(this.defaults);
        } else {
            this.settings = this.setAll(this.defaults);
            return this.settings;
        }
    }

    _mergeDefaults(defaults: any) {
        for (const k in defaults) {
            if (!(k in this.settings)) {
                this.settings[k] = defaults[k];
            }
        }
        return this.setAll(this.settings);
    }

    merge(settings: any) {
        for (const k of settings) {
            this.settings[k] = settings[k];
        }
        return this.save();
    }

    setValue(key: string, value: any) {
        this.settings[key] = value;
        return localStorage.setItem(this.SETTINGS_KEY, JSON.stringify(this.settings));
    }

    private setAll(value: PreferencesModel) {
        localStorage.setItem(this.SETTINGS_KEY, JSON.stringify(value))
        return value;
    }

    getValue(key: string) {
        const settings = JSON.parse(localStorage.getItem(this.SETTINGS_KEY));
        return settings[key];
    }

    save() {
        return this.setAll(this.settings);
    }

    get allSettings() {
        return this.settings;
    }
}

