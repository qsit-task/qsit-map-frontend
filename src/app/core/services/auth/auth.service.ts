import {Injectable} from '@angular/core';
import {SharedPreferencesService} from '@core/services';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private sharedPreferencesService: SharedPreferencesService) {
    }

    isAuthenticated() {
        this.sharedPreferencesService.load();
        return this.sharedPreferencesService.allSettings.apiToken != null && this.sharedPreferencesService.allSettings.apiToken !== "";
    }
}
