import {NgModule} from '@angular/core';
import {Configuration} from '@core/http/configuration';
import {ApiModule} from '@core/http/api.module';
import {AppSettingsConfig} from '@app-config';
import {AuthService, UserPreferencesServiceProvider} from '@core/services';

@NgModule({
    imports: [
        ApiModule.forRoot(() => new Configuration({
            basePath: AppSettingsConfig.APIS_BASE_PATH
        }))
    ],
    declarations: [],
    exports: [],
    providers: [
        AuthService,
        UserPreferencesServiceProvider
    ]
})
export class CoreModule {
}
