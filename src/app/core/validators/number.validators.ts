import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export class NumberValidators {

    static isNumberCheck(maxDecimals: number = 3): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            const regx = new RegExp('^\\d+(\\.\\d{1,' + maxDecimals + '})?$');
            const number = regx.test(control.value) ? +control.value : NaN;
            if (number !== number) {
                return {isNumberCheck: true};
            }
            return null;
        }
    }
}
