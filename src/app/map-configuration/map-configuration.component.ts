import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MapConfiguration} from '@core/http/models/mapConfiguration';
import {AppConstants} from '@app-config';
import {NumberValidators} from '@core/validators/number.validators';
import {MapConfigService} from '@core/http/api/mapConfig.service';

@Component({
    selector: 'app-map-configuration',
    templateUrl: './map-configuration.component.html',
    styleUrls: ['./map-configuration.component.css']
})
export class MapConfigurationComponent implements OnInit {
    mapTypes = [
        {
            id: 1,
            name: 'Features'
        },
        {
            id: 2,
            name: 'Basemap'
        }
    ];
    mapSubTypes = [
        {
            id: 1,
            name: 'Dynamic',
            mapTypeId: 1,
        },
        {
            id: 2,
            name: 'Cached',
            mapTypeId: 1,
        },
        {
            id: 3,
            name: 'Imagery',
            mapTypeId: 2,
        },
        {
            id: 4,
            name: 'Topographic',
            mapTypeId: 2,
        }
    ];
    selectedMapTypes = [];

    mapForm: FormGroup;

    validationMessages = {
        clusterRadius: [
            {type: 'required', message: 'Cluster Radius is required.'},
            {type: 'min', message: 'Zero is Not allowed, the minimum value is 0.01'},
            {type: 'max', message: 'Maximum number is 99.999.'},
            {type: 'isNumberCheck', message: 'Cluster Radius must be a number, and max 3 decimal places.'}
        ],
        geoFencing: [
            {type: 'required', message: 'Geo Fencing is required.'}
        ],
        timeBuffer: [
            {type: 'required', message: 'Time Buffer is required.'},
            {type: 'min', message: 'Zero is Not allowed, the minimum value is 0.01'},
            {type: 'max', message: 'Maximum number is 99.999.'},
            {type: 'isNumberCheck', message: 'Cluster Radius must be a number, and max 3 decimal places.'}
        ],
        locationBuffer: [
            {type: 'required', message: 'Location Buffer is required.'},
            {type: 'min', message: 'Zero is Not allowed, the minimum value is 0.01'},
            {type: 'max', message: 'Maximum number is 99.999.'},
            {type: 'isNumberCheck', message: 'Cluster Radius must be a number, and max 3 decimal places.'}
        ],
        duration: [
            {type: 'required', message: 'Duration is required.'},
            {type: 'min', message: 'Zero is Not allowed, the minimum value is 0.01'},
            {type: 'max', message: 'Maximum number is 99.999.'},
            {type: 'isNumberCheck', message: 'Cluster Radius must be a number, and max 3 decimal places.'}
        ],
        mapType: [
            {type: 'required', message: 'Map Type is required.'}
        ],
        mapSubType: [
            {type: 'required', message: 'Map Sub Type is required.'}
        ]
    };

    mapConfiguration: MapConfiguration = {};

    constructor(private mapConfigService: MapConfigService) {
    }

    ngOnInit() {
        this.mapForm = new FormGroup({
            clusterRadius: new FormControl(this.mapConfiguration.clusterRadius, Validators.compose([
                Validators.required,
                NumberValidators.isNumberCheck(),
                Validators.max(99.999),
                Validators.min(0.001),
            ])),
            geoFencing: new FormControl(this.mapConfiguration.geoFencing),
            timeBuffer: new FormControl(this.mapConfiguration.timeBuffer, Validators.compose([
                Validators.required,
                NumberValidators.isNumberCheck(4),
                Validators.max(99.999),
                Validators.min(0.001),
            ])),
            locationBuffer: new FormControl(this.mapConfiguration.locationBuffer, Validators.compose([
                Validators.required,
                NumberValidators.isNumberCheck(),
                Validators.max(99.999),
                Validators.min(0.001),
            ])),
            duration: new FormControl(this.mapConfiguration.duration, Validators.compose([
                Validators.required,
                NumberValidators.isNumberCheck(),
                Validators.max(99.999),
                Validators.min(0.001),
            ])),
            mapType: new FormControl(this.mapConfiguration.mapTypeId, Validators.compose([
                Validators.required,
            ])),
            mapSubType: new FormControl(this.mapConfiguration.mapSubTypeId, Validators.compose([
                Validators.required,
            ])),
        });
    }

    onChange(value: number) {
        console.log(value);
        this.selectedMapTypes = this.mapSubTypes.filter(e => e.mapTypeId === value);
    }

    submit() {
        if (this.mapForm.valid) {
            this.mapConfiguration = {
                clusterRadius: this.mapForm.get('clusterRadius').value,
                geoFencing: this.mapForm.get('geoFencing').value ? this.mapForm.get('geoFencing').value : false,
                timeBuffer: this.mapForm.get('timeBuffer').value,
                locationBuffer: this.mapForm.get('locationBuffer').value,
                duration: this.mapForm.get('duration').value,
                mapTypeId: this.mapForm.get('mapType').value,
                mapSubTypeId: this.mapForm.get('mapSubType').value
            };
            this.mapConfigService.apiMapconfigPost(this.mapConfiguration).subscribe(result => {
                if (result.id > 0) {
                    alert('Data saved successfully');
                }
            }, error => {
                const serverError = error.error;
                console.error(serverError.errors);
                alert(`Server error! ${error.message}:${serverError.message}, please try again later`);
            });
        } else {
            console.log('form-not-valid ', this.mapForm.get('clusterRadius').errors);
        }
    }
}
