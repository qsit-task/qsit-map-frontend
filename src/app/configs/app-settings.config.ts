import {environment} from '../../environments/environment';

export class AppSettingsConfig {
    static readonly APP_BASE_PATH = environment.base_url;
    static readonly APIS_BASE_PATH = AppSettingsConfig.APP_BASE_PATH + '/api';
}
