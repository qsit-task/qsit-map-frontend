export class AppConstants {
    static readonly ROUTES = {
        LOGIN: 'login',
        CONFIGURATION: 'map-configuration',
        MAP: 'maps'
    };
}
