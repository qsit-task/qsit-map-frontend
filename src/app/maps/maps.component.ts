import {
    Component,
    OnInit,
    ViewChild,
    ElementRef,
    OnDestroy,
} from '@angular/core';

import WebMap from '@arcgis/core/WebMap';
import MapView from '@arcgis/core/views/MapView';
import Bookmarks from '@arcgis/core/widgets/Bookmarks';
import Expand from '@arcgis/core/widgets/Expand';
import FeatureLayer from '@arcgis/core/layers/FeatureLayer';
import MapImageLayer from '@arcgis/core/layers/MapImageLayer';
import LayerView from '@arcgis/core/views/layers/LayerView';
import FeatureTable from '@arcgis/core/widgets/FeatureTable';
import Zoom from '@arcgis/core/widgets/Zoom';
import * as reactiveUtils from '@arcgis/core/core/reactiveUtils';
import FieldColumnTemplate from '@arcgis/core/widgets/FeatureTable/support/FieldColumnTemplate';

@Component({
    selector: 'app-maps',
    templateUrl: './maps.component.html',
    styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit, OnDestroy {

    // The <div> where we will place the map
    @ViewChild('mapViewNode', {static: true}) private mapViewEl!: ElementRef;
    @ViewChild('mapTableNode', {static: true}) private mapTableViewEl!: ElementRef;
    webMap: WebMap;
    damageAssessmentLayer: FeatureLayer;
    layerView: LayerView;
    mapView: MapView;
    featureTable: FeatureTable;
    features = [];

    initializeMap(): Promise<any> {
        const container = this.mapViewEl.nativeElement;
        const tableContainer = this.mapTableViewEl.nativeElement;
        this.features = [];

        this.damageAssessmentLayer = new FeatureLayer({
            url: 'https://sampleserver6.arcgisonline.com/arcgis/rest/services/DamageAssessmentStatePlane/MapServer/0',
            title: 'Damage to Residential Buildings'
        });

        this.webMap = new WebMap({
            basemap: 'topo-vector',
            layers: [this.damageAssessmentLayer]
        });

        this.layerView = new LayerView({
            visible: true,
        });

        this.mapView = new MapView({
            container,
            map: this.webMap,
            center: [-88.152971, 41.773283],
            zoom: 16,
            popup: {
                autoOpenEnabled: false
            }
        });

        this.mapView.layerViews.add(this.layerView);

        // Create the feature table
        this.featureTable = new FeatureTable({
            layer: this.damageAssessmentLayer,
            view: this.mapView, // required for feature highlight to work
            visibleElements: {
                header: true,
                menu: true,
                menuItems: {
                    clearSelection: true,
                    refreshData: true,
                    toggleColumns: true,
                    selectedRecordsShowAllToggle: true,
                    // showSelectedToggle: true,
                    zoomToSelection: true,
                    showSelectedToggle: true
                },
                selectionColumn: true,
                columnMenus: true
            },
            tableTemplate: { // autocast to TableTemplate
                columnTemplates: [ // takes an array of FieldColumnTemplate and GroupColumnTemplate
                    {
                        type: 'field',
                        fieldName: 'incidentnm',
                        label: 'Incident Name',
                        direction: 'asc'
                    },
                    {
                        type: 'field',
                        fieldName: 'inspdate',
                        label: 'Inspection Date Time',
                        direction: 'asc'
                    },
                    {
                        type: 'field',
                        fieldName: 'firstname',
                        label: 'Contact First Name',
                        direction: 'asc'
                    },
                    {
                        type: 'field',
                        fieldName: 'objectid',
                        label: 'Object ID',
                        direction: 'asc'
                    }
                ]
            },
            container: tableContainer
        });

        this.webMap.when(() => {
            // Listen for the table's selection-change event
            this.featureTable.on('selection-change', (changes) => {
                // If the selection is removed, remove the feature from the array
                changes.removed.forEach((item) => {
                    const data = this.features.find(current => {
                        return current.feature === item.feature;
                    });
                    if (data) {
                        this.features.splice(this.features.indexOf(data), 1);
                    }
                });

                // If the selection is added, push all added selections to array
                changes.added.forEach((item) => {
                    const feature = item.feature;
                    this.features.push({
                        feature: feature
                    });
                });

                this.zoomToSelectedFeature(this.damageAssessmentLayer, this.mapView);
            });

            // Listen for the click on the view and select any associated row in the table
            this.mapView.on('immediate-click', (event) => {
                this.mapView.hitTest(event).then((response) => {
                    const candidate: any = response.results.find((result: any) => {
                        return (
                            result.graphic &&
                            result.graphic.layer &&
                            result.graphic.layer === this.damageAssessmentLayer
                        );
                    });
                    // Select the rows of the clicked feature
                    this.featureTable.selectRows(candidate.graphic);
                });
            });
        });
        return this.mapView.when();
    }

    ngOnInit(): any {
        // Initialize MapView and return an instance of MapView
        this.initializeMap().then(() => {
            // The map has been initialized
            console.log('The map is ready.');
        });
    }

    ngOnDestroy(): void {
        if (this.mapView) {
            // destroy the map view
            this.mapView.destroy();
        }
    }

    onChange(value: any) {
        const expression = `objectid < ${value}`;
        console.log(expression);
        this.damageAssessmentLayer.definitionExpression = expression;
    }

    zoomToSelectedFeature(featureLayer: FeatureLayer, view: MapView) {
        // Create a query off of the feature layer
        const query = featureLayer.createQuery();
        // Iterate through the features and grab the feature's objectID
        const featureIds = this.features.map(function (result) {
            return result.feature.getAttribute(featureLayer.objectIdField);
        });
        // Set the query's objectId
        query.objectIds = featureIds;
        // Make sure to return the geometry to zoom to
        query.returnGeometry = true;
        // Call queryFeatures on the feature layer and zoom to the resulting features
        featureLayer.queryFeatures(query).then(function (results) {
            view.goTo(results.features).catch(function (error) {
                if (error.name !== 'AbortError') {
                    console.error(error);
                }
            });
        });
    }
}
