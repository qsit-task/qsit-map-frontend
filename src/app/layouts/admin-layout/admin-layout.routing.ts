import {Routes} from '@angular/router';

import {MapsComponent} from '../../maps/maps.component';
import {MapConfigurationComponent} from '../../map-configuration/map-configuration.component';
import {AuthGuard} from '@core/guards/auth-guard.service';

export const AdminLayoutRoutes: Routes = [
    {
        path: 'map-configuration',
        component: MapConfigurationComponent
    },
    {
        path: 'maps',
        component: MapsComponent
    },
];
