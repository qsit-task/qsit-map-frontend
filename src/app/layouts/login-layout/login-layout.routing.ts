import { Routes } from '@angular/router';

import { UserLoginComponent } from '../../user-login/user-login.component';

export const LoginLayoutRoutes: Routes = [
    { path: 'login',   component: UserLoginComponent },
];
