import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoginLayoutComponent } from './layouts/login-layout/login-layout.component';
import {AuthGuard} from '@core/guards/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'map-configuration',
    pathMatch: 'full',
    canActivate: [AuthGuard],
  }, {
    path: '',
    component: AdminLayoutComponent,
    children: [{
      path: '',
      loadChildren: () => import('./layouts/admin-layout/admin-layout.module').then(m => m.AdminLayoutModule)
    }],
    canActivate: [AuthGuard]
  }, {
    path: '',
    component: LoginLayoutComponent,
    children: [{
      path: '',
      loadChildren: () => import('./layouts/login-layout/login-layout.module').then(m => m.LoginLayoutModule)
    }]
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
       useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
