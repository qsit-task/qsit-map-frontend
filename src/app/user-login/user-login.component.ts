import {Component, Inject, OnInit} from '@angular/core';
import {UserService} from '@core/http/api';
import {UserCredentials} from '@core/http/models';
import {SharedPreferencesService} from '@core/services';
import {Router} from '@angular/router';
import {AppConstants} from '@app-config';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DOCUMENT} from '@angular/common';

@Component({
    selector: 'app-user-login',
    templateUrl: './user-login.component.html',
    styleUrls: ['./user-login.component.scss']
})
export class UserLoginComponent implements OnInit {

    userCredentials: UserCredentials = {
        clientId: '',
        state: ''
    };
    loginForm: FormGroup;

    validationMessages = {
        username: [
            {type: 'required', message: 'Email or Username is required.'}
        ],
        password: [
            {type: 'minlength', message: 'Minimum length is 4'},
            {type: 'required', message: 'Password is required.'},
        ]
    };

    constructor(
        private userService: UserService,
        private sharedPreferences: SharedPreferencesService,
        private router: Router,
        @Inject(DOCUMENT) private document: Document,
    ) {
    }

    ngOnInit() {
        this.loginForm = new FormGroup({
            username: new FormControl(this.userCredentials.username, Validators.compose([
                Validators.required,
                // Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new FormControl(this.userCredentials.password, Validators.compose([
                Validators.minLength(4),
                Validators.required
            ]))
        });
    }

    login() {
        if (this.loginForm.valid) {
            this.userCredentials.username = this.loginForm.get('username').value;
            this.userCredentials.password = this.loginForm.get('password').value;
            this.userService.signIn(this.userCredentials).subscribe(result => {
                if (result.status) {
                    console.log(result);
                    this.sharedPreferences.setValue('apiToken', result.access_token);
                    this.router.navigate([AppConstants.ROUTES.CONFIGURATION]).then();
                } else {
                }
            }, (error) => {

            });
        } else {
            console.log('form-not-valid ', this.loginForm);
        }
    }
}
